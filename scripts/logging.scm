
;;;     Copyright 2022 Li-Cheng (Andy) Tai
;;;                      atai@atai.org
;;;
;;;     gde_appmenu is free software: you can redistribute it and/or modify it
;;;     under the terms of the GNU General Public License as published by the Free
;;;     Software Foundation, either version 3 of the License, or (at your option)
;;;     any later version.
;;;
;;;     gde_appmenu is distributed in the hope that it will be useful, but WITHOUT
;;;     ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;;;     FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
;;;     more details.
;;;
;;;     You should have received a copy of the GNU General Public License along with
;;;     gde_appmenu. If not, see http://www.gnu.org/licenses/.

(define-module (scripts logging)
  #:use-module (logging logger)
  #:use-module (logging port-log)
  #:use-module (oop goops)
  #:export (setup-logging
            shutdown-logging

            ))

                                        ; bsed on guile-log documentation, https://www.nongnu.org/guile-lib/doc/ref/logging.logger/
(define (setup-logging)
  (let ((lgr       (make <logger>))
        (err       (make <port-log> #:port (current-error-port))))
    ;; don't want to see debug on the screen!!
    (disable-log-level! err 'DEBUG)

    ;; add the handlers to our logger
    (add-handler! lgr err)

    ;; make this the application's default logger
    (set-default-logger! lgr)
    (open-log! lgr)))


(define (shutdown-logging)
  (flush-log)   ;; since no args, it uses the default
  (close-log!)  ;; since no args, it uses the default
  (set-default-logger! #f))
