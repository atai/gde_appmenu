dnl -*- Autoconf -*-

define(GDE_APPMENU_CONFIGURE_COPYRIGHT,[[

Copyright 2022 Li-Cheng (Andy) Tai
atai@atai.org

This file is part of gde_appmenu

gde_appmenu is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version.

gde_appmenu is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public
License along with gde_appmenu.  If not, see
<https://www.gnu.org/licenses/gpl.html>.

]])


AC_INIT(gde_appmenu, 0.1.2)
AC_SUBST(VERSION, "\"0.1.2\"")
AC_SUBST(AUTHOR, "\"Andy Tai\"")
AC_SUBST(COPYRIGHT, "'(2022)")
AC_SUBST(LICENSE, gpl3+)
AC_COPYRIGHT(GDE_APPMENU_CONFIGURE_COPYRIGHT)
AC_CONFIG_SRCDIR(scripts/gde_appmenu.scm)
AC_CONFIG_AUX_DIR([build-aux])
AM_INIT_AUTOMAKE([1.12 gnu silent-rules subdir-objects  color-tests parallel-tests -Woverride -Wno-portability])
AM_SILENT_RULES([yes])

PKG_CHECK_MODULES([G_GOLF], [g-golf-1.0])
AC_SUBST([G_GOLF_LD_PATH], `${PKG_CONFIG} g-golf-1.0 --variable=libdir`)
AC_SUBST([GUILE_LOAD_PATH], `${PKG_CONFIG} g-golf-1.0 --variable=sitedir`)
AC_SUBST([GUILE_LOAD_COMPILED_PATH], `${PKG_CONFIG} g-golf-1.0 --variable=siteccachedir`)

AC_CONFIG_FILES([Makefile])
AC_CONFIG_FILES([pre-inst-env], [chmod +x pre-inst-env])

dnl Search for 'guile' and 'guild'.  This macro defines
dnl 'GUILE_EFFECTIVE_VERSION'.
GUILE_PKG([3.0])
GUILE_PROGS
GUILE_SITE_DIR
if test "x$GUILD" = "x"; then
   AC_MSG_ERROR(['guild' binary not found; please check your guile-3.x installation.])
fi

dnl Hall auto-generated guile-module dependencies


dnl Installation directories for .scm and .go files.
guilemoduledir="${datarootdir}/guile/site/$GUILE_EFFECTIVE_VERSION"
guileobjectdir="${libdir}/guile/$GUILE_EFFECTIVE_VERSION/site-ccache"
AC_SUBST([guilemoduledir])
AC_SUBST([guileobjectdir])

AC_OUTPUT
