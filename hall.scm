(hall-description
  (name "gde_appmenu")
  (prefix "")
  (version "0.1.2")
  (author "Andy Tai")
  (copyright (2022))
  (synopsis "")
  (description "")
  (home-page "")
  (license gpl3+)
  (dependencies `())
  (files (libraries ())
         (tests ((directory "tests" ())))
         (programs
           ((directory
              "scripts"
              ((scheme-file "gde_appmenu")))))
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "gclip-select")))))
         (infrastructure
           ((scheme-file "guix") (scheme-file "hall")))))
